import React, { Component } from "react"
import Card from 'react-bootstrap/Card';
import { LinkContainer } from "react-router-bootstrap";

class ListaFacturas extends Component
{
    constructor()
    {
        super()
        this.state = {
            facturas: []
        }
    }
    
    componentDidMount()
    {
        fetch("http://localhost:3001/api/facturas")
        .then(res => res.json())
        .then((data) => {
            
            this.setState({
                facturas: data
            })
            
        })
    }
    
    render()
    {
        
        let facList = this.state.facturas.map(factura => {
            return (
                    <Card key={factura.nroFactura} style={{ width: '18rem', display: 'inline-block', margin:'10px' }}>
                      <Card.Body>
                        <Card.Title>{factura.nroFactura}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">{factura.fechaEmision}</Card.Subtitle>
                        <Card.Text> Cond. de pago: {factura.condPago}
                        </Card.Text>
                        <LinkContainer to={"/facturas/" + factura.nroFactura}>
                            <Card.Link>Ver Factura</Card.Link>
                        </LinkContainer>
                      </Card.Body>
                    </Card>
            );
            
        });
        
        return (
        
          <div >
                {facList}        
            </div>
            
        );
    }
}


export default ListaFacturas