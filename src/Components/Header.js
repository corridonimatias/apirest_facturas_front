import React, { Component } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';
import { LinkContainer } from "react-router-bootstrap";

function Header() {
    return (
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="/">Site de Facturas</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <LinkContainer to="/">
                    <Nav.Link >Listado</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/clientes">
                    <Nav.Link >Clientes</Nav.Link>
                </LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
    );
  }


export default Header;
