import React, { Component } from "react"
import Card from 'react-bootstrap/Card';
import { LinkContainer } from "react-router-bootstrap";

class ListaFacturas extends Component
{
    constructor()
    {
        super()
        this.state = {
            factura: {}
        }
    }
    
    componentDidMount()
    {
        fetch("http://localhost:3001/api/facturas/" + this.props.match.params.id)
        .then(res => res.json())
        .then((data) => {
            
            this.setState({
                factura: data
            })
            
        })
    }
    
    render()
    {
        
        let items = this.state.factura.item ? this.state.factura.item.map(item => 
                            {
                                return <li>{item.cantidad} de {item.producto} a ${item.precio} cada uno</li>        
                            }) : [];
        return (
        
          <div style={{margin: "20px"}}>
                <h1>Detalles para la factura nro {this.state.factura.nroFactura}</h1>
                
                <ul>
                    <li>Emision: {this.state.factura.fechaEmision}</li>
                    <li>Vencimiento: {this.state.factura.fechaVencimiento}</li>
                    <li>Items:</li>
                    <ul>
                        {
                            items
                        }
                    </ul>
                </ul>
            </div>
            
        );
    }
}


export default ListaFacturas