import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'

import Header from './Components/Header'
import ListaFacturas from './Components/ListaFacturas'
import Factura from './Components/Factura'


function Error()
{
    return <div>404</div>
}

class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <div>
                <Header />
                <Switch>
                    <Route path="/" component={ListaFacturas} exact />
                    <Route path="/facturas/:id" component={Factura} exact />
                    <Route component={Error} />
                </Switch>
            </div>
        </BrowserRouter>
    );
  }
}

/*

<div>
    <Header />
    <ListaFacturas />
</div>

*/

export default App;
